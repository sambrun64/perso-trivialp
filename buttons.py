class Bouton:
    def __init__(self,fenetre,image):
        self.fenetre = fenetre
        self.sprite_on = fenetre.set_logo(image)
        self.est_actif = False
        self.rect = self.sprite_on.get_rect()

    def set_rect(self,x,y):
        self.set_x(x)
        self.set_y(y)

    def set_x(self,x):
        self.rect.x = x

    def set_y(self,y):
        self.rect.y = y

    def check_actif(self,event):
        if self.rect.collidepoint(event):
            self.est_actif = True
        return self.est_actif