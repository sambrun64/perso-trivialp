import random

class Game:
    def __init__(self):
        self.mode = 'start'
        self.sous_mode = None   #theme
        self.est_question = True
        self.question = None
        self.reponse = None
        self.proposition = None

        #fichiers question
        self.file_geo = "assets/questions/questions_geo.txt"
        self.file_divert = "assets/questions/questions_divert.txt"
        self.file_hist = "assets/questions/questions_hist.txt"
        self.file_art = "assets/questions/questions_art.txt"
        self.file_science = "assets/questions/questions_science_nature.txt"
        self.file_sport = "assets/questions/questions_sport.txt"

        #faire la fonction:
        self.question_geo = self.init_question_list(self.read_nb_quest(self.file_geo))
        self.question_divert = self.init_question_list(self.read_nb_quest(self.file_divert))
        self.question_hist = self.init_question_list(self.read_nb_quest(self.file_hist))
        self.question_art = self.init_question_list(self.read_nb_quest(self.file_art))
        self.question_science = self.init_question_list(self.read_nb_quest(self.file_science))
        self.question_sport = self.init_question_list(self.read_nb_quest(self.file_sport))

    def read_nb_quest(self,file):
        f = open(file,'r',encoding='utf-8')
        l1 = f.readline()
        f.close()
        if (l1 == ''):
            print('Attention Erreur lecture dans fichier : ' +file)
            return 0
        else:
            return int(l1)

    def init_question_list(self,nb):
        l = []
        if nb != 0:
            for i in range(1,nb+1):
                l.append(i)
            random.shuffle(l)
        return l

    def trouver_question(self,chemin,numero):
        compteur = 0
        for line in open(chemin, "r"):
            if compteur == (4*numero) -2 :
                self.question = str(numero) + ': ' + line.replace('\n','')
            if compteur == (4*numero)-1:
                self.proposition = line.replace('\n','')
            if compteur == (4*numero):
                self.reponse = line.replace('\n','')
            compteur += 1

    def lire_question(self):
        if self.sous_mode == 'geographie':
            if len(self.question_geo)<1:
                print('plus de questions dans : Géographie')
                self.question_geo = self.init_question_list(self.read_nb_quest(self.file_geo))
            num_quest = self.question_geo.pop()
            self.trouver_question(self.file_geo, num_quest)
        elif self.sous_mode == 'divertissement':
            if len(self.question_divert)<1:
                print('plus de questions dans : Divertissement')
                self.question_divert = self.init_question_list(self.read_nb_quest(self.file_divert))
            num_quest = self.question_divert.pop()
            self.trouver_question(self.file_divert, num_quest)
        elif self.sous_mode == 'histoire':
            if len(self.question_hist)<1:
                print('plus de questions dans : Histoire')
                self.question_hist = self.init_question_list(self.read_nb_quest(self.file_hist))
            num_quest = self.question_hist.pop()
            self.trouver_question(self.file_hist, num_quest)
        elif self.sous_mode == 'art_et_litterature':
            if len(self.question_art)<1:
                print('plus de questions dans : Art et Littérature')
                self.question_art = self.init_question_list(self.read_nb_quest(self.file_art))
            num_quest = self.question_art.pop()
            self.trouver_question(self.file_art, num_quest)
        elif self.sous_mode == 'science_et_nature':
            if len(self.question_science)<1:
                print('plus de questions dans : Science et Nature')
                self.question_science = self.init_question_list(self.read_nb_quest(self.file_science))
            num_quest = self.question_science.pop()
            self.trouver_question(self.file_science, num_quest)
        elif self.sous_mode == 'sport_et_loisir':
            if len(self.question_sport)<1:
                print('plus de questions dans : Sport et Loisir')
                self.question_sport = self.init_question_list(self.read_nb_quest(self.file_sport))
            num_quest = self.question_sport.pop()
            self.trouver_question(self.file_sport, num_quest)

    def set_theme(self,theme):
        self.theme = theme

    def switch_mode(self,mode):
        self.mode = mode
        if mode == 'theme':
            self.est_question = True

    def switch_sous_mode(self,sous_mode):
        self.sous_mode = sous_mode
