import pygame
from windows import Window
from game import Game

#a faire :
#gestion de l'erreur avec mode erreur
#affichage du texte a revoir
#chek si la liste des quest est vide => affichage de rechargement
#gestion multifichier si bcp de quest et long a ouvrir

pygame.init()

#Gestion de la fenètre:
game = Game()
fenetre = Window(game)
screen = fenetre.screen

#Musique
sound = pygame.mixer.Sound('assets/Music-fond-1.wav')
sound.play(loops=-1, maxtime=0, fade_ms=0)

#Boucle du jeu :
continuer = True
while continuer:
    #mettre a jour l'écran
    pygame.display.flip()

    if game.mode == 'start':
        pygame.mixer.unpause()
    else:
        pygame.mixer.pause()

    #test de sortie pour fin de programme :
    for event in pygame.event.get():
        #vérif fin de jeu
        if event.type == pygame.QUIT:
            continuer = False
        #verif changement de taille
        if event.type == pygame.VIDEORESIZE:
            fenetre.resize(event,game.mode,game.sous_mode,game.question)
            fenetre.update_fond(game.mode)
        #verif début de partie
        if event.type == pygame.MOUSEBUTTONDOWN:
            if game.mode == 'start':
                if fenetre.start_button.check_actif(event.pos):
                    game.switch_mode('choose')
                    fenetre.start_button.est_actif = False
            elif game.mode == 'choose':
                if fenetre.back_button.check_actif(event.pos):
                    game.switch_mode('start')
                    fenetre.back_button.est_actif = False
                else:
                    if fenetre.geographie_button.check_actif(event.pos):
                        game.switch_mode('theme')
                        game.switch_sous_mode('geographie')
                        fenetre.geographie_button.est_actif = False
                    elif fenetre.divertissement_button.check_actif(event.pos):
                        game.switch_mode('theme')
                        game.switch_sous_mode('divertissement')
                        fenetre.divertissement_button.est_actif = False
                    elif fenetre.histoire_button.check_actif(event.pos):
                        game.switch_mode('theme')
                        game.switch_sous_mode('histoire')
                        fenetre.histoire_button.est_actif = False
                    elif fenetre.art_et_litterature_button.check_actif(event.pos):
                        game.switch_mode('theme')
                        game.switch_sous_mode('art_et_litterature')
                        fenetre.art_et_litterature_button.est_actif = False
                    elif fenetre.science_et_nature_button.check_actif(event.pos):
                        game.switch_mode('theme')
                        game.switch_sous_mode('science_et_nature')
                        fenetre.science_et_nature_button.est_actif = False
                    elif fenetre.sport_et_loisir_button.check_actif(event.pos):
                        game.switch_mode('theme')
                        game.switch_sous_mode('sport_et_loisir')
                        fenetre.sport_et_loisir_button.est_actif = False
                    game.lire_question()
                    #game.question = 'test question : èéà'
                    #game.proposition = ''
                    #game.reponse = "c'est la grande question àèé"
            elif game.mode == 'theme':
                if fenetre.next_button.check_actif(event.pos):
                    if game.est_question:
                        game.est_question = False
                    else:
                        fenetre.text = game.reponse
                        game.switch_mode('choose')
                    fenetre.next_button.est_actif = False

            fenetre.reload_windows()
            fenetre.update_fond(game.mode,game.sous_mode,game.est_question)

#Fin du programme
sound.stop()
print(fenetre.largeur,fenetre.hauteur)
pygame.mixer.stop()
pygame.quit()


