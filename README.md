# Trivial Pursuit Project

**This project has an educational objective. It is not intended to be shared or commercialized.**


This is my first "big" personal project. Many things are wrong with this code. 

## Setup
This project requires a version of python higher than 3.7

### Pygames Library
Pygames is required, here is a link to an install page : 
https://www.pygame.org/wiki/GettingStarted
Or via : 
> python3 -m pip install -U pygame --user

## Give a try : 
> python3 main.py

### Issues
encoding problems on the questions may appear !
