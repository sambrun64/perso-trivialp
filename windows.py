import pygame
import math
from buttons import Bouton

class Window:
    def __init__(self,game):
        #init de la fenetre
        pygame.display.set_caption("Trivial Pursuit")
        pygame.display.set_icon(pygame.image.load('assets/Logo_haut_page.png'))
        self.screen = pygame.display.set_mode((1250,650),pygame.RESIZABLE)
        self.largeur = self.screen.get_width()
        self.hauteur = self.screen.get_height()

        #init du jeu
        self.game = game

        #getion image/logo
        self.background_start = self.set_image('assets/Fond_start.jpg')
        self.background_choose = self.set_image('assets/Background_choose.jpg')
        self.background_geographie = self.set_image('assets/Background_geographie.jpg')
        self.background_divertissement = self.set_image('assets/Background_divertissement.jpg')
        self.background_histoire = self.set_image('assets/Background_histoire.jpg')
        self.background_art_et_litterature = self.set_image('assets/Background_art_et_litterature.jpg')
        self.background_science_et_nature = self.set_image('assets/Background_science_et_nature.jpg')
        self.background_sport_et_loisirs = self.set_image('assets/Background_sport_et_loisir.jpg')
        self.logo = self.set_logo('assets/logo_trivial_pursuit.png')

        #Boutons de la pages
        self.start_button = Bouton(self,'assets/Play_button.png')
        self.back_button = Bouton(self,'assets/Back_icon.png')
        self.next_button = Bouton(self,'assets/Bouton_suivant.png')

        #boutons de themes
        self.geographie_button = Bouton(self, 'assets/Geographie.jpg')
        self.divertissement_button = Bouton(self, 'assets/Divertissements.jpg')
        self.histoire_button = Bouton(self, 'assets/Histoire.jpg')
        self.art_et_litterature_button = Bouton(self, 'assets/Art_et_litterature.jpg')
        self.science_et_nature_button = Bouton(self, 'assets/Science_et_nature.jpg')
        self.sport_et_loisir_button = Bouton(self, 'assets/Sport_et_loisir.jpg')

        #Initialisation du texte
        self.taille_police = 36
        self.type_texte = pygame.font.SysFont('freesans', self.taille_police)

        #initialisation du premier fond
        self.update_fond('start')

    #charge une image et la renvoie
    def set_image(self,chemin):
        return pygame.image.load(chemin).convert()

    #charge un logo est le renvoie
    def set_logo(self,chemin):
        return pygame.image.load(chemin).convert_alpha()

    #gere l'affichage des differents fond
    def update_fond(self,etat,sous_mode=None,question=True):
        #applique le fond de la fenetre
        if etat == 'start':
            self.afficher_start()
        elif etat == 'choose':
            self.afficher_choose()
        else:
            self.afficher_theme(sous_mode,question)

    def afficher_theme(self,sous_mode,est_question):#,mode
        # fond:
        if sous_mode == 'geographie':
            self.afficher(self.background_geographie,(self.largeur, self.hauteur))
            self.dessine_rect('assets/bleu.jpg',70)
        elif sous_mode == 'divertissement':
            self.afficher(self.background_divertissement, (self.largeur, self.hauteur))
            self.dessine_rect('assets/rose.jpg',100)
        elif sous_mode == 'histoire':
            self.afficher(self.background_histoire, (self.largeur, self.hauteur))
            self.dessine_rect('assets/jaune.jpg',70)
        elif sous_mode == 'art_et_litterature':
            self.afficher(self.background_art_et_litterature, (self.largeur, self.hauteur))
            self.dessine_rect('assets/marron.jpg',150)
        elif sous_mode == 'science_et_nature':
            self.afficher(self.background_science_et_nature, (self.largeur, self.hauteur))
            self.dessine_rect('assets/vert.jpg',100)
        elif sous_mode == 'sport_et_loisir':
            self.afficher(self.background_sport_et_loisirs, (self.largeur, self.hauteur))
            self.dessine_rect('assets/orange.jpg',60)
        else:
            pass
        #affichage du bouton suivant:
        self.next_button.set_rect(math.ceil(4*self.largeur / 5),math.ceil(3.5*self.hauteur / 5))
        self.afficher(self.next_button,(math.ceil(self.largeur / 5),math.ceil(self.hauteur / 4.5)),posi=self.next_button.rect,est_button=True)
        if est_question:
            self.afficher_question(self.game.question,0)
            self.afficher_prop(self.game.proposition)
        else:
            self.afficher_question(self.game.question, 0)
            self.afficher_reponse(self.game.reponse)

    #fixe le texte
    def set_texte(self,text):
        self.text = self.type_texte.render(text, False, (250, 250, 250))
        self.text_pos = self.text.get_rect()

    def set_texte_pos(self,posy):
        self.text_pos.centerx = self.screen.get_rect().centerx
        self.text_pos.centery = posy

    def afficher_question(self,txt,passage):
        if len(txt)*4*self.taille_police > 4*self.largeur/5:
            list_txt = txt.split()
            txt_1 = ''
            txt_2 = ''
            for i in range(0,len(list_txt)):
                if i < math.ceil((12 / 1300) * self.largeur):
                    txt_1 = txt_1 + ' ' + list_txt[i]
                else:
                    txt_2 = txt_2 + ' ' + list_txt[i]
            self.set_texte(txt_1)
            self.set_texte_pos(math.ceil(self.hauteur / 5) + passage*(self.taille_police+30))
            self.screen.blit(self.text, self.text_pos)
            self.afficher_question(txt_2,passage+1)
        else:
            self.set_texte(txt)
            self.set_texte_pos(math.ceil(self.hauteur / 5) + passage*(self.taille_police+30))
            self.screen.blit(self.text, self.text_pos)

    def dessine_rect(self,chemin_fond,alph):
        image = self.set_image(chemin_fond)
        nv_image = pygame.transform.scale(image,(self.largeur,self.hauteur))
        nv_image.set_alpha(alph)
        self.screen.blit(nv_image,(0,0))

    def afficher_prop(self,txt):
        if txt != '':
            liste_txt = txt.split(',')
            for i in range(0,len(liste_txt)):
                if liste_txt[i][0] == ' ':
                    liste_txt[i] = liste_txt[i][1:]
                self.set_texte(liste_txt[i])
                self.set_texte_pos(math.ceil((self.hauteur/2.5) + i * (self.taille_police + 30)))
                self.screen.blit(self.text, self.text_pos)

    def afficher_reponse(self,txt):
        self.set_texte('Réponse :')
        self.set_texte_pos(math.ceil(2 * self.hauteur / 3))
        self.screen.blit(self.text, self.text_pos)
        self.set_texte(txt)
        self.set_texte_pos(math.ceil(2.3*self.hauteur/3))
        self.screen.blit(self.text, self.text_pos)

    def afficher_choose(self):
        #fond:
        self.afficher(self.background_choose,(self.largeur, self.hauteur))
        #bouton retour:
        self.back_button.set_rect(math.ceil(self.largeur - self.largeur/13),math.ceil(self.hauteur - self.hauteur/7))
        self.afficher(self.back_button,(math.ceil(self.largeur / 13), math.ceil(self.hauteur / 7)),posi=self.back_button.rect,est_button=True)
        #Nom du jeu:
        pos = (math.ceil((self.largeur - self.largeur / 4) / 2) - 5,math.ceil(self.hauteur / 20))
        self.afficher(self.logo,(math.ceil(self.largeur / 4), math.ceil(self.hauteur / 6)),posi=pos)
        #Bouton Geographie:
        self.geographie_button.rect = self.geographie_button.sprite_on.get_rect(center=self.geographie_button.rect.center)
        self.geographie_button.set_rect(math.ceil(self.largeur/8),math.ceil(self.hauteur/4))
        self.afficher(self.geographie_button,(math.ceil(self.largeur / 7), math.ceil(self.hauteur / 6)),posi=self.geographie_button.rect,est_button=True)
        # Bouton divertissement:
        self.divertissement_button.rect = self.divertissement_button.sprite_on.get_rect(center=self.divertissement_button.rect.center)
        self.divertissement_button.set_rect(math.ceil(self.largeur / 2 - self.largeur/12), math.ceil(self.hauteur / 4))
        self.afficher(self.divertissement_button, (math.ceil(self.largeur / 7), math.ceil(self.hauteur / 6)),posi=self.divertissement_button.rect, est_button=True)
        # Bouton histoire:
        self.histoire_button.rect = self.histoire_button.sprite_on.get_rect(center=self.histoire_button.rect.center)
        self.histoire_button.set_rect(math.ceil(5.68*self.largeur / 8), math.ceil(self.hauteur / 4))
        self.afficher(self.histoire_button, (math.ceil(self.largeur / 7), math.ceil(self.hauteur / 6)),posi=self.histoire_button.rect, est_button=True)
        #Bouton art et littérature
        self.art_et_litterature_button.rect = self.art_et_litterature_button.sprite_on.get_rect(center=self.art_et_litterature_button.rect.center)
        self.art_et_litterature_button.set_rect(math.ceil(self.largeur / 8), math.ceil(self.hauteur /1.8))
        self.afficher(self.art_et_litterature_button, (math.ceil(self.largeur / 7), math.ceil(self.hauteur / 6)),posi=self.art_et_litterature_button.rect, est_button=True)
        #Bouton Science et nature
        self.science_et_nature_button.rect = self.science_et_nature_button.sprite_on.get_rect(center=self.science_et_nature_button.rect.center)
        self.science_et_nature_button.set_rect(math.ceil(self.largeur / 2 - self.largeur / 12),math.ceil(self.hauteur / 1.8))
        self.afficher(self.science_et_nature_button, (math.ceil(self.largeur / 7), math.ceil(self.hauteur / 6)),posi=self.science_et_nature_button.rect, est_button=True)
        #bouton sport et loisirs
        self.sport_et_loisir_button.rect = self.sport_et_loisir_button.sprite_on.get_rect(center=self.sport_et_loisir_button.rect.center)
        self.sport_et_loisir_button.set_rect(math.ceil(5.68 * self.largeur / 8), math.ceil(self.hauteur / 1.8))
        self.afficher(self.sport_et_loisir_button, (math.ceil(self.largeur / 7), math.ceil(self.hauteur / 6)),posi=self.sport_et_loisir_button.rect, est_button=True)

    def afficher_start(self):
        # affichage du fond
        self.afficher(self.background_start,(self.largeur, self.hauteur))
        # affichage du titre
        posit = (math.ceil((self.largeur - self.largeur / 4) / 2) - 5, math.ceil(self.hauteur / 4))
        self.afficher(self.logo,(math.ceil(self.largeur / 4), math.ceil(self.hauteur / 6)),posi = posit)
        # affichage bouton
        self.start_button.set_rect(math.ceil((self.largeur - self.largeur / 4) / 2),math.ceil(self.hauteur / 2))
        self.afficher(self.start_button,(math.ceil(self.largeur / 4), math.ceil(self.hauteur / 8)), posi = self.start_button.rect,est_button=True)

    def est_fenetré(self):
        #verif si les images ont besoins d'ètre redimensionné
        return self.largeur == 1366 and self.hauteur == 705

    #affiche obj_a_affiche a la posi et avec l'echelle taille
    def afficher(self,obj_a_affiche,coef_taille,posi=(0,0),est_button=False):
        if est_button:
            if (not self.est_fenetré()):
                self.logo_affiche = pygame.transform.scale(obj_a_affiche.sprite_on, coef_taille)
            else:
                self.logo_affiche = obj_a_affiche.sprite_on
            obj_a_affiche.rect = self.logo_affiche.get_rect()
            obj_a_affiche.set_rect(posi[0], posi[1])

        else:
        # si on est pas en fenetré il faut changer la taille
            if (not self.est_fenetré()):
                self.logo_affiche = pygame.transform.scale(obj_a_affiche,coef_taille)
            else:
                self.logo_affiche = obj_a_affiche
        self.screen.blit(self.logo_affiche,posi)

    def resize(self,event,mode,sous_mode,question):
        #change la taille de l'écran
        self.largeur = event.w
        self.hauteur = event.h
        self.screen = pygame.display.set_mode((self.largeur, self.hauteur), pygame.RESIZABLE)
        self.update_fond(mode,sous_mode,question)

    def reload_windows(self):
        self.screen = pygame.display.set_mode((self.largeur, self.hauteur), pygame.RESIZABLE)